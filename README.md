# Gitting set up

1. [Install git](http://git-scm.com/downloads), if it isn't already (check by opening a Terminal and typing `git`).

2. Tell git who you are:

        $ git config --global user.name "Noam Chomsky"
        $ git config --global user.email "chomsky@mit.edu"
        # optional, if you know what you're doing: 
        $ git config --global core.editor "nano"

    **Note:** any time you see code with a `$`, that means you can run everything that comes after the `$` in a terminal.  Lines starting with `#` are comments that shouldn't be run.

3. Optional: if you don't already have a favorite plain-text editor, download the [Atom text editor](https://atom.io/), a nice, easy to use editor that integrates with git well.  To set it as the default editor for git, run

        $ git config --global core.editor "atom --wait"

4. [Create an account on Bitbucket](https://bitbucket.org/account/signup/).  If you use a `.edu` email address you get a free, unlimited account.

5. Optional: [set up Bitbucket with your SSH keys](https://confluence.atlassian.com/display/BITBUCKET/How+to+install+a+public+key+on+your+Bitbucket+account).  This will save you from typing in your password a lot.

# Tutorial workflows:

1. [Track changes to your files locally.](workflow-00-add-commit.md)
2. [Share changes with pull requests.](workflow-01-fork-pull-request.md)
3. [Keep a lab notebook with multiple branches](workflow-02-lab-note-book.md)

# Git basics

## What is git?

* git is a _version control system_.
* Like Dropbox, it tracks changes in files on your computer over time.
* Unlike Dropbox, it only tracks changes that you tell it to track, when you tell it to track them.

## Important vocabulary:

* A _repository_: a collection of files that are under version control, plus the history of changes to those files.
* A _commit_: a snapshot of a repository. Also a set of changes to a repository.
    * A commit has ancestors and descendants, in terms of changes, a commit represents a set of changes to one or more ancestors.
* A _branch_: is a series of commits.
* A _head_: the most recent commit in a branch.