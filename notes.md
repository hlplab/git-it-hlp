# Demos/Workflows

0. [Dave] Basic local workflow: 
    * Create a git repository anywhere with `git init`.
    * Edit a file and add it to the repository with `git add`.
    * (repeat)
    * Save a snapshot of the current version with `git commit`
        * `git commit -m 'your commit message here'`
        * `git commit -a` -m 'your commit message here' : add all changes and then commit.
1. [Dave] Fork/pull request workflow: no conflicts
    * Esteban:
        * To start: fork and clone demo repo.
        * Make some edits, add + commit + push.
        * Open pull request.
    * Dave:
        * Look at pull request, comment, etc.
        * Merge pull request.
        * Pull from remote to synchronize
    * Everyone: pair up and do the same thing.
2. [Dave] Fork/pull request workflow: parallel, with conflicts
    * Dave:
        * Make some changes, commit, push.
    * Esteban: 
        * Make other changes (conflicting), commit, push.
        * Open pull request
    * Everyone: Pair up and do the same thing.
3. [Esteban] Managing project for yourself ("lab notebook")
4. [Esteban/Dave] (If we have time) One repo, many branches model
    * Create branches
    * Make pull requests between branches.


# Notes

* What is git?
    * A _version control system_, specifically a _distributed_ one.
    * Keeps track of changes to a collection of files.
* Why for collaboration?
    * Explicit tracking of who changed what when (and why)
    * Comment on and update proposed changes via pull requests
    * History is safe (you can undo changes)
* Why for own project?
    * Keep track of changes + why/when you made them, in a way that is easy to view and search later.
    * Backup to the cloud
    * Freedom to experiment: easily create alternative versions, go back in time, etc. by switching branches.
    * Super easy to share with anyone later: just push the repo to bitbucket etc.
