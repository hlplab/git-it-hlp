# Workflow 1: Sharing is caring

With git, the way you share with others is by making a copy of their repository, commit some changes in your copy, and send a **pull request** to the owner of the original.

This is usually handled by cloud services like [Bitbucket](https://bitbucket.org/) and [Github](https://github.com).  These are really just big servers that let you download ("pull") and upload ("push") repositories, plus some web-based tools for managing the copies that are stored on their servers.  One of the most useful is the **pull request**, which allows you to ask that someone pull in changes you've made.

## An example

Let's say Dave and Esteban are working together on a [tutorial on using git](https://bitbucket.org/dkleinschmidt/git-it-hlp/).  Dave is the owner of the repository on Bitbucket, and Esteban wants to suggest some changes to the tutorial.

### Dave: upload repository to Bitbucket

1. Dave wants to share his tutorial with some other people, so he [creates an empty repository on Bitbucket](https://bitbucket.org/repo/create).

2. Now Dave needs to upload, or **push** the repository on his computer to the Bitbucket servers.  This is a two-step process.
    1. First, tell git where the remote copy lives:

            $ cd /path/to/my/repo
            $ git remote add origin git@bitbucket.org:dkleinschmidt/git-it-hlp.git

    2. Then push everything in the local copy to Bitbucket:

            $ git push -u origin --all

3. Finally, Dave can share the copy of the repository he uploaced to Bitbucket with Esteban.
    * If Dave created it as a _public_ repository, this is as simple as sending Esteban the repository link.  
    * If it's a private repository, Dave needs to grant Esteban read access, by clicking on the "Share" button and entering Esteban's username or email and clicking "add".

        ![](img/share-button.png)

4. If Esteban just wants to look at the project, he can download, or **clone**, the shared repository

    * Esteban can click on the handy "Clone" button on Bitbucket: 

        ![Click "clone" in the left-hand sidebar](img/clone.png "Click 'clone' in the left-hand bar")

        Bitbucket gives Esteban a cut-and-paste-able snippet that will be something like

            $ git clone https://ebuz@bitbucket.org/dkleinschmdit/git-it-hlp.git
        
    * Esteban opens a terminal and pastes this command.  git will download Esteban's copy of the repository from Bitbucket into a directory of the same name.

            Cloning into 'git-it-hlp'...
            remote: Counting objects: 37, done.
            remote: Compressing objects: 100% (34/34), done.
            remote: Total 37 (delta 10), reused 0 (delta 0)
            Receiving objects: 100% (37/37), 7.07 KiB | 0 bytes/s, done.
            Resolving deltas: 100% (10/10), done.
            Checking connectivity... done.

5. Then if Dave makes any changes, Esteban can get the latest version by just running

        $ git pull

### Esteban: fork and clone

1. If Esteban wants to actually contribute changes, he needs to **fork** the [repository](https://bitbucket.org/dkleinschmidt/git-it-hlp) to his account, so that he can write to it:

    ![Click "fork" in the left-hand side-bar, or type `xf`](img/forkit.png "Click 'fork' in the left-hand bar")

    Bitbucket will ask for a name for the forked copy, think for a second, and then Esteban's own, freshly forked [copy of the original repository](https://bitbucket.org/ebuz/git-it-hlp/) will load.

2. Esteban then either
    1. **Clones** his own fork into a new copy of the repository on his computer, just like above, or
    2. If Esteban already cloned the original copy of the repository, he can tell git to send any new commits he makes to his forked copy, or, in git terms, add his new fork as a **remote**:

            $ cd /location/of/previous/clone
            $ git remote add myfork https://ebuz@bitbucket.org/ebuz/git-it-hlp.git

        but replacing the URL with whatever the URL is in the top-right corner of the forked repository page:

        ![](img/repo-url.png)

### Esteban: Share some changes

At this point, Esteban's fork (copy) is identical to Dave's original.  Let's say Esteban has some ideas about how to improve the tutorial.

1. Esteban edits some files in the repository, and [adds/commits them](workflow-00-add-commit.md):

        $ git add .
        $ git commit -m 'my brilliant ideas'

2. Esteban pushes his changes to Bitbucket

        $ git push

    This uploads the commits Esteban has made to his copy stored on the server.

3. Now, Esteban's and Dave's copies of the repository are different.  Esteban can request that Dave **pull** his commits into Dave's copy by opening a **pull request**.

    ![Click "Create pull request" on the left-hand sidebar](img/create-pull-req.png "Click 'Create pull request'")

    Optionally, you can
    * Change the title and enter a message in the description box.
    * Review the commits that will be merged if the pull request is accepted (the "Commits" tab all the way at the bottom), and see the corresponding changes to the actual files (the "Diff" tab).
    * Assign particular Bitbucket users to review or sign off on the pull request.

    Then click the "Create pull request" button at the bottom to open the request and send it.

### Dave: Review pull request

1. Now Dave will receive a notification that there's an open pull request on his repository from Bitbucket.

2. Dave goes to the page for his repository and checks the [pull requests page](https://bitbucket.org/dkleinschmidt/git-it-hlp/pull-requests)

    ![](img/pull-req-link.png)

3. On the page for the pull request, Dave can review the proposed changes.  Dave and Esteban can also comment on the pull request, 
    * As a whole
    * On an individual file
    * On an individual line that was changed
   Both Dave and Esteban will be able to see and respond to these comments.

4. If Esteban wants to update the pull request, possibly in response to the conversation with Dave (adding new changes, or undoing some proposed changes), he repeats the steps above, of edit files, `git add`, `git commit`, and `git push`.  Bitbucket will update the pull request automatically.

5. When Dave approves of the suggested changes, he can click the "Merge" button at the top to accept the pull request and add the proposed changes to his own copy of the respository.

## Now you try

This workflow works best with a partner: the **sender** plays the role of Esteban, who's suggesting changes to a repository owned by the **receiver**, who plays the role of Dave.

1. The **receiver**: 
    * Create a repository on Bitbucket, and push some changes to it.  You can either
    * Start with a repository on your computer, or 
    * Fork an existing Bitbucket repository and clone it to your computer. A good repository to fork is [the tutorial repository itself](https://bitbucket.org/hlplab/git-it-hlp/)!
    * (optionally) Make some changes, then push (upload) the changes to your fork:

            $ git add .
            $ git commit
            $ git push

    * Send the **sender** the link to your repository on Bitbucket, using the "Share" button

2. The **sender**: once you get the notification from Bitbucket that the sender has shared their repository with you: 
    * Open it up on Bitbucket, and fork a copy for yourself.  **Make sure you fork the receiver's copy, not the original**.  (This is a quirk of Bitbucket).
    * Once your fork is ready, clone your forked copy to your computer.
    * Create a new **branch** for your changes so that it's clear you have diverged from the "master" branch, and make that branch active with `checkout`: 

            $ git branch my-brilliant-ideas
            $ git checkout my-brilliant-ideas

    * Make some changes on your computer to the copy you just cloned, and then upload them to your forked copy of the repository on Bitbucket.

            $ git add .
            $ git commit
            $ git push

3. Now the sender can open a pull request to ask the reciever to incorporate their suggested changes.

    * Start the process by clicking "Create pull request" in the left-hand side bar:

        ![](img/create-pull-req.png "Click 'Create pull request'")

    * First check that the _source_ of the pull request is right: select the branch you created from the drop-down menu on the left-side box

        ![](img/pull-req-source.png)

    * Then confirm that the _destination_ (right-side box) of the pull request is the **receiver**'s fork (which should be the default)

        ![](img/pull-req-dest.png)

    * Also confirm that the changes you made are actually included in the pull request, by looking at the bottom of the screen.  If it says "There are no changes" you probably haven't pushed your changes successfully.
    * Finally, enter a descriptive title and (optionally) a longer message describing what is contained in the pull request and why, and click the big blue "Create pull request" button.

4. The receiver should get a notification from Bitbucket that a new pull request has been opened.
    * On the Bitbucket page for their fork, there shoud be a little "1" next to the "Pull requests" button on the left.  Click on that button and then on the pull request on the list that should come up.
    * Now the receiver can view the pull request and the proposed changes, and comment on them.
    * Leave a couple of comments on specific changes.

5. Now, the sender and receiver can have a back-and-forth about the proposed changes.  If the sender wants to add or revise anything, all they need to do is repeat the original steps of making changes and then pushing those changes to their fork:

        # ...make changes to files...
        $ git add .
        $ git commit
        $ git push

    ...and the pull request will be automatically updated.

6. Once the sender and receiver agree that everything is good to go, the receiver can accept the pull request by **merging** it into their own repository, by clicking the big blue "merge" button at the top right.

    ![](img/merge.png)

7. Lastly, the receiver can update the local copy of their repository on their actual computer by running `git pull`.  The sender already has all the changes, but if they want to update again later they can do one of two things: 
    * Click on the "update now" button on the right side of the Bitbucket page for their fork.
    * Pull the latest version directly from the receiver's copy by running 

            $ git pull <reciever's url>

        where the URL is taken from the Bitbucket page for the receiver's repository:

        ![](img/repo-url.png)

## Bonus details

### Track changes

This workflow has a lot in common with the Word track changes workflow, except that it splits out _commentary_ on proposed changes (or existing content) from the files that are changed themselves.  Merging a pull request is similar to accepting all changes on a document.

### Where to send the request

By default, Bitbucket will request that the original repository owner pull the changes from your fork's `master` branch into their `master` branch.  You can change both the source branch---and the destination repository and branch---using the drop-down menus in the pull request creation screen.

### Conflict management

This workflow assumes that Dave doesn't make any changes at all while Esteban is working.  Of course, this isn't very realistic.  The good news is that, as long as Dave and Esteban don't make any _conflicting_ changes, git can merge them automatically.

When there _is_ a conflicting change, Dave will need to tell git how to resolve the conflict.  To do this, he has to manually merge Esteban's version into his own.

Dave starts by pulling the changes from Esteban's repository (in the branch `my-brilliant-feature`) to the repository on his own computer:

    $ git pull https://ebuz@bitbucket.org/ebuz/git-it-hlp.git my-brilliant-feature

This will produce a lot of very unhappy sounding output.  But all Dave needs to do is open up the files that git tells him are conflicted in his favorite text editor, fix the conflicts, and then

    $ git add .
    $ git commit
    $ git push

This will automatically close the pull request
