1. Write workflow modules
    0. [Dave] Basic local workflow: 
    1. [Dave] Fork/pull request workflow: no conflicts
        * Instructions for DIY parts
    2. [Dave] Fork/pull request workflow: parallel, with conflicts
    3. [Esteban] Managing project for yourself ("lab notebook")
    4. [Esteban/Dave] (If we have time) One repo, many branches model
2. Move concepts in notes to README.
